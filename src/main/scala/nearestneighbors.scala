/*
import org.apache.spark.mllib.recommendation.ALS
import org.apache.spark.mllib.recommendation.MatrixFactorizationModel
import org.apache.spark.mllib.recommendation.Rating
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql.SQLContext._
import org.apache.spark.sql.SparkSession
import org.apache.spark.ml.feature.{ HashingTF, IDF, Tokenizer }
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.ml.feature.Normalizer
import org.apache.spark.ml.feature.DCT
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.linalg.distributed.RowMatrix

import org.apache.log4j.{ Level, Logger }
import scala.util.Random

object NearestNeighbors {

  def addScoreToAggregate(a: (Double, Int), s: Double) = {
    (a._1 + s, a._2 + 1)
  }

  def addAggregates(a: (Double, Int), b: (Double, Int)) = {
    (a._1 + b._1, a._2 + b._2)
  }
  def main(args: Array[String]) {

    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)
    Logger.getLogger("INFO").setLevel(Level.OFF)
    val conf = new SparkConf().setMaster("local[6]").setAppName("Collaborative filtering using content-based nearest neighbors")
    val sc: SparkContext = new SparkContext(conf)

    // Load and parse the data 
    val ratings = sc.textFile("data_ml/ratings.csv").filter(!_.startsWith("u")).map(_.split(',') match {
      case Array(user, movie, rate, time) =>
        Rating(user.toInt, movie.toInt, rate.toDouble)
    })
    ratings.cache()

    val avgRatingPerUser = ratings.map(r => (r.user, r.rating)).aggregateByKey((0d, 0))(addScoreToAggregate, addAggregates).mapValues(a => a._1 / a._2).collect().toMap
    val goodRatings = null//TODO
    val movieGenres = null //TODO
    val movieNames = null //TODO
    val userVectors = goodRatings.flatMap(r => movieGenres(r.product).map(g => ((r.user, g), 1))).reduceByKey(_ + _).map(t => (t._1._1, (t._1._2, t._2))).groupByKey().map(x => (x._1, x._2.toMap))
    userVectors.take(5).foreach(println)
    //at this point, we have in user vectors tuples where the key is user ID and values are Map[genre, freq]
    //from this we can compute a distance between two users

    //here potential to add IDF
    def userSim(u1: Map[String, Int], u2: Map[String, Int]): Double = {
      0.
    }

    val testUser = Map(
      "Romance" -> 10,
      "Comedy" -> 5,
      "Mystery" -> 5)
    val knn = userVectors.map(u => (userSim(u._2, testUser), u._1)).top(10)
    knn.foreach(println)
    val knnSet = knn.map(_._2).toSet
    val recom = goodRatings.filter(r => knnSet.contains(r.user)).map(r => (r.product, 1)).reduceByKey(_ + _).map(_.swap).top(5)
    recom.foreach(r => println(movieNames(r._2) +"\t" + r._1 ))
  }
}*/
