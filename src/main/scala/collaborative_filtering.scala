
import org.apache.spark.mllib.recommendation.ALS
import org.apache.spark.mllib.recommendation.MatrixFactorizationModel
import org.apache.spark.mllib.recommendation.Rating
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.log4j.{Level, Logger}
import org.apache.spark.ml.tuning.{ParamGridBuilder, TrainValidationSplit}
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}
import org.apache.spark.rdd.RDD
import org.apache.spark.util.SizeEstimator

import scala.util.Random

object collaborative_filtering {
  def addScoreToAggregate(a: (Double, Int), s: Double) = {
    (a._1 + s, a._2 + 1)
  }

  def addAggregates(a: (Double, Int), b: (Double, Int)) = {
    (a._1 + b._1, a._2 + b._2)
  }

  def main(args: Array[String]) {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)
    Logger.getLogger("INFO").setLevel(Level.OFF)
    val conf = new SparkConf().setMaster("local[6]").setAppName("Collaborative filtering using ALS")

    val sc: SparkContext = new SparkContext(conf)

    // Load and parse the data
    val data = sc.textFile("data/ratings_small.csv")
    val ratings = data.filter(!_.startsWith("u")).map(_.split(',') match {
      case Array(user, movie, rate, time) =>
        Rating(user.toInt, movie.toInt, rate.toDouble)
    })
    ratings.cache()

    val avgRatingPerUser = ratings.map(r => (r.user, r.rating)).aggregateByKey((0d, 0))(addScoreToAggregate, addAggregates).mapValues(a => a._1 / a._2).collect().toMap

    val ratings_norm = ratings.map { case Rating(user, item, rate) => Rating(user, item, rate - avgRatingPerUser(user)) }

    // Prepare training and test data.
    val Array(training, test) = ratings_norm.randomSplit(Array(0.9, 0.1), seed = 12345)

    training.cache()
    test.cache()
    //    we use it again later
    //    ratings.unpersist(false)

    val numTraining = training.count()
    val numTest = test.count()

    println("Training: " + numTraining + ", test: " + numTest)

    val rank = 12
    val numIterations = 10
    val lambda = 0.01

    val model = ALS.train(training, rank, numIterations, lambda)
    //load pretrained model
    //    val model = MatrixFactorizationModel.load(sc, "target/tmp/myCollaborativeFilterN")
    println("done training")

    // Evaluate the model on rating data
    val usersProducts = test.map {
      case Rating(user, product, rate) =>
        (user, product)
    }

    val p = model.predict(1, 200)
    println("Prediction " + p)
    val predictions =
      model.predict(usersProducts).map {
        case Rating(user, product, rate) =>
          ((user, product), rate)
      }

    val ratesAndPreds = test.map {
      case Rating(user, product, rate) =>
        ((user, product), rate)
    }.join(predictions)

    //    We only use it once
    //    ratesAndPreds.cache()

    val MSE = ratesAndPreds.map {
      case ((user, product), (r1, r2)) =>
        val err = (r1 - r2)
        err * err
    }.mean()
    println("Mean Squared Error = " + MSE)

    // save the trained model
    // model.save(sc, "target/tmp/myCollaborativeFilterN")

    // get the most popular artists

    //1re question
    val moviesData = sc.textFile("data/movies.csv")
    val moviesRDD:RDD[(Int, String)] = moviesData.filter(!_.startsWith("m")).map(x=>(x.split(","))).map(x=> (x(0).toInt, x(1)))//TODO
    val movies = moviesRDD.collectAsMap()

    //2è question
    val mostRatedMovies: Set[Int] = data.filter(!_.startsWith("u")).map(_.split(',') match {
        case Array(user, movie, rate, time) =>
          (user.toInt, movie.toInt)
      }).map(x => (x._2, 1)).reduceByKey(_+_).sortByKey(false).top(200).map {
      case (movie, userNomber) =>
        (movie)
    }.toSet//TODO

    //val selectedMovies:List[(Int,String)] =
    val shuffleMovies:Set[Int] = Random.shuffle(mostRatedMovies).take(40)//
    val selectedMovies:List[(Int,String)] = moviesRDD.filter(x => shuffleMovies.contains(x._1)).collect().toList

    //    selectedMovies.foreach(println)

    // this one should go in another file or somewhere else, but i need it defined before I use it
    /** Elicitate ratings from commandline **/
    def elicitateRatings(movies: Seq[(Int, String)]) = {
      var sumRatings = 0
      val prompt = "Rate each movie 1-5, 0 if you haven't seen it:"
      println(prompt)
      val ratings = movies.flatMap { x =>

        var rating: Option[Rating] = None
        var valid = false

        while (!valid) {
          print(x._2 + ": ")
          try {
            val r = scala.io.StdIn.readInt()
            if (r < 0 || r > 5) {
              println(prompt)
            } else {
              valid = true
              if (r > 0) {
                sumRatings += r
                rating = Some(Rating(0, x._1, r))
              }
            }
          } catch {
            case e: Exception => println(prompt)
          }
        }

        rating match {
          case Some(r) => Iterator(r)
          case None    => Iterator.empty
        }

      } //end flatMap

      if (ratings.isEmpty) {
        sys.error("No rating provided")
      } else {
        if (sumRatings > 0) {
          ratings.map(r => Rating(r.user, r.product, r.rating - (sumRatings / ratings.size)))
        } else {
          ratings
        }
      }
    }

    // prompt for rating on most popular artists
    val myRatings = elicitateRatings(selectedMovies)
    val myRatingsRDD = sc.parallelize(myRatings)
    // val rated_artists = ratings.map(_.product).distinct().collect().toSet
    // val exclude = rated_artists.++(myRatedArtistsIds)

    val myRatedArtistsIds = myRatings.map(_.product).toSet

    // set of candidate artists, exclude the ones already rated
    //    val candidates = sc.parallelize(movies.keys.filter(!myRatedArtistsIds.contains(_)).toSeq)

    //extend training dataset with the added ratings and retrain the model (get the one from optimal params)) 
    val extended_trainset = training.++(myRatingsRDD)
    val model2 = ALS.train(extended_trainset, rank, numIterations, lambda)

    // predict ratings for candidate artists and recommend the top 5
    val recommendations = model2.recommendProducts(0, 10)

    var i = 1
    println("Movies recommended for you:")
    recommendations.foreach { r =>
      println("%2d".format(i) + ": " + movies(r.product))
      i += 1
    }

  }

}