
package recommendersystem

object ALSParameterTuning {

  import org.apache.spark.mllib.recommendation.ALS
  import org.apache.spark.mllib.recommendation.MatrixFactorizationModel
  import org.apache.spark.mllib.recommendation.Rating
  import org.apache.spark.SparkContext
  import org.apache.spark.SparkContext._
  import org.apache.spark.SparkConf

  import org.apache.log4j.{ Level, Logger }

  import org.apache.spark.ml.tuning.{ ParamGridBuilder, TrainValidationSplit }
  import org.apache.spark.ml.tuning.{ CrossValidator, ParamGridBuilder }
  import org.apache.spark.util.SizeEstimator
  import scala.util.Random

  def addScoreToAggregate(a: (Double, Int), s: Double) = {
    (a._1 + s, a._2 + 1)
  }

  def addAggregates(a: (Double, Int), b: (Double, Int)) = {
    (a._1 + b._1, a._2 + b._2)
  }

  def main(args: Array[String]) {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)
    Logger.getLogger("INFO").setLevel(Level.OFF)
    val conf = new SparkConf().setMaster("local[6]").setAppName("Collaborative filtering using ALS")

    val sc: SparkContext = new SparkContext(conf)

    // Load and parse the data
    val data = sc.textFile("data/ratings_small.csv")
    val ratings = data.filter(!_.startsWith("u")).map(_.split(',') match {
      case Array(user, movie, rate, time) =>
        Rating(user.toInt, movie.toInt, rate.toDouble)
    })
    ratings.cache()

    val avgRatingPerUser = ratings.map(r => (r.user, r.rating)).aggregateByKey((0d, 0))(addScoreToAggregate, addAggregates).mapValues(a => a._1 / a._2).collect().toMap

    val ratings_norm = ratings.map { case Rating(user, item, rate) => Rating(user, item, rate - avgRatingPerUser(user)) }

    // Prepare training and test data.
    val Array(training, test) = ratings_norm.randomSplit(Array(0.9, 0.1), seed = 12345)

    training.cache()
    test.cache()

    val numTraining = training.count()
    val numTest = test.count()

    println("Training: " + numTraining + ", test: " + numTest)

    //training a model with some random parameters
    val rank = 5
    val numIterations = 10
    val lambda = 1

    val model = ALS.train(training, rank, numIterations, lambda)
    println("done training")

    // Evaluate the model on test data
    val usersProducts = test.map {
      case Rating(user, product, rate) =>
        (user, product)
    }

    val predictions =
      model.predict(usersProducts).map {
        case Rating(user, product, rate) =>
          ((user, product), rate)
      }

    val ratesAndPreds = test.map {
      case Rating(user, product, rate) =>
        ((user, product), rate)
    }.join(predictions)

    val MSE = ratesAndPreds.map {
      case ((user, product), (trueRating, predictedRating)) =>
        val err = (trueRating - predictedRating)
        err * err
    }.mean()
    println("Mean Squared Error = " + MSE)

    // select optimal parameters
    var bestValidationRmse = Double.MaxValue
    var bestRank = 0
    var bestLambda = -1.0
    var bestNumIter = -1

    // modify the values of ranks, lambdas and numIters
    val ranks = List(8, 12)
    val lambdas = List(0.01, 1, 10)
    val numIters = List(10, 20)

    for (rank <- ranks; lambda <- lambdas; numIter <- numIters) {

      val model = ALS.train(training, rank, numIter, lambda)
      val usersProducts = test.map { case Rating(user, product, rate) => (user, product) }
      val predictions = model.predict(usersProducts).map { case Rating(user, product, rate) => ((user, product), rate) }
      val ratesAndPreds = test.map { case Rating(user, product, rate) => ((user, product), rate) }.join(predictions)
      val validationRmse = ratesAndPreds.map {
        case ((user, product), (r1, r2)) =>
          val err = (r1 - r2)
          err * err
      }.mean()

      println("RMSE (validation) = " + validationRmse + " for the model trained with rank = "
        + rank + ", lambda = " + lambda + ", and numIter = " + numIter + ".")

      if (validationRmse < bestValidationRmse) {
        bestValidationRmse = validationRmse
        bestRank = rank
        bestLambda = lambda
        bestNumIter = numIter
      }
    }

    println("The best model was trained with rank = " + bestRank + " and lambda = " + bestLambda
      + ", and numIter = " + bestNumIter + ", and its RMSE on the test set is " + bestValidationRmse + ".")

  }
}